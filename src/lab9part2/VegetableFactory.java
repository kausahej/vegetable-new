/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9part2;

/**
 *
 * @author sahej
 */
enum Type {
   BEET, CARROT;
}
public class VegetableFactory {
     private static VegetableFactory vegtableFactory = null;

   private VegetableFactory() {

   }

   public static VegetableFactory getInstance() {
       if (vegtableFactory == null) {
           vegtableFactory= new VegetableFactory();
       }
       return vegtableFactory;
   }

   public Beet getBeet(Type type) {
       if (type == Type.BEET) {
           return new Beet("pink", 5, "Beet") {
               @Override
               boolean isRipe() {
                   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
               }
           };
       } else if (type == Type.CARROT) {
           return new Carrot("red", 3, "Carrot");
       }
       return null;
   }
}