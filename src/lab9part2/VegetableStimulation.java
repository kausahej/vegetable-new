/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9part2;

/**
 *
 * @author sahej
 */
import java.util.ArrayList;

public class VegetableStimulation {

public static void main(String args[]){
       VegetableFactory vegetableFactory = VegetableFactory.getInstance();
       Beet beet = vegetableFactory.getBeet(Type.BEET);
       System.out.println(beet.describeBeet());
     
       Beet beet2 = vegetableFactory.getBeet(Type.CARROT);
       System.out.println(beet2.describeBeet());
      

   }
}